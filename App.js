import React from 'react';
import { useFonts } from 'expo-font';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer, DefaultTheme } from '@react-navigation/native';
import HomeScreen from './screens/HomeScreen';
import DetailScreen from './screens/DetailScreen';
import SignUpScreen from './screens/SignUpScreen';
import HeaderLogo from './shared/HeaderLogo'
import CartLogo from './shared/CartLogo'
import MenuLogo from './shared/MenuLogo';
const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();

const options = ({ navigation, route }) => ({
  headerTitleAlign: 'center',
  headerStyle: {
    elevation: 0
  },
  headerTitle: props => <HeaderLogo {...props} />,
  headerLeft: (props) => <MenuLogo {...props} onPress={() => navigation.openDrawer()} />,
  headerRight: (props) => <CartLogo {...props} />,
});
const DrawerHomeRoutes = () => {
  return (
    <Stack.Navigator initialRouteName="Home">
      <Stack.Screen name='Home' component={HomeScreen} options={options} />
      <Stack.Screen name='Detail' component={DetailScreen} options={{
        headerTitleAlign: 'center',
        headerTitle: props => <HeaderLogo {...props} />,
        headerRight: (props) => <CartLogo {...props} />,
        headerStyle: {
          elevation: 0
        },
        headerTransparent: true,
      }} />
    </Stack.Navigator>
  )
}
const DrawerSignInRoutes = () => {
  return (
    <Stack.Navigator initialRouteName="SignUp" >
      <Stack.Screen name='SignUp' component={SignUpScreen} options={{
        headerTitleAlign: 'center',
        headerTitle: props => <HeaderLogo {...props} />,
        headerStyle: {
          elevation: 0
        }
      }}
      />
    </Stack.Navigator>
  )
}
const DrawerDetailRoutes = () => {
  return (
    <Stack.Navigator initialRouteName="Detail" >
      <Stack.Screen name='Detail' component={DetailScreen} options={{
        headerTitleAlign: 'center',
        headerTitle: props => <HeaderLogo {...props} />,
        headerRight: (props) => <CartLogo {...props} />,
        headerStyle: {
          elevation: 0
        },
      }} />
    </Stack.Navigator>
  )
}
export default function App() {
  const [loaded] = useFonts({
    nunitoLight: require('./assets/fonts/Nunito-Light.ttf'),
    nunitoRegular: require('./assets/fonts/Nunito-Regular.ttf'),
    nunitoSemiBold: require('./assets/fonts/Nunito-SemiBold.ttf'),
    nunitoBold: require('./assets/fonts/Nunito-Bold.ttf'),
  });

  if (!loaded) {
    return null;
  }
  return (
    <NavigationContainer theme={DefaultTheme}>
      <Drawer.Navigator initialRouteName='SignUp'>
        <Drawer.Screen name="Home" component={DrawerHomeRoutes} />
        <Drawer.Screen name="SignUp" component={DrawerSignInRoutes} />
        <Drawer.Screen name="Detail" component={DrawerDetailRoutes} />
      </Drawer.Navigator>
    </NavigationContainer>
  );
}