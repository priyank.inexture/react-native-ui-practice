import styled, { css } from 'styled-components/native'

const Container = styled.View`
    flex: 1;
    background-color: white;
    ${props => props.center && css`
        justify-content: center;
        align-items: center;
    `}
    ${props => props.spaceBetween && css`
        justify-content: space-between;
    `}
    ${props => props.spaceEvenly && css`
        justify-content: space-evenly;
    `}
    ${props => props.row && css`
        flex-direction: row;
    `}
    ${props => props.column && css`
        flex-direction: column;
    `}
    padding: ${props => props.paddingVertical || 0}px ${props => props.paddingHorizontal || 0}px;
`
const InfoContainer = styled.View`    
    padding: 0 24px;
`
const AmazonHeaderLogo = styled.Image`
    width: 200px;
    height: 20px;
    margin-top: 5px;
    resize-mode: contain;
`
const CartHeaderLogo = styled.Image`
    width: 20px;
    height: 20px;
    margin-top: 0px;
    margin-right: 15px;
    resize-mode: contain;
`
export { Container, InfoContainer, AmazonHeaderLogo, CartHeaderLogo }