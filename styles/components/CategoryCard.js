import styled from 'styled-components/native';

const CategorCardContainer = styled.View`
    flex: 1;
    padding-bottom: 25px;
    justify-content: center;
    align-items: center;
`;
const CategoryImage = styled.Image`
    
`;
export { CategorCardContainer, CategoryImage }