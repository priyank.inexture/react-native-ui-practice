import styled from 'styled-components/native';

const SearchContainer = styled.View`    
  flex:1;
  flex-direction: row;
  justify-content: space-evenly;
  background: #f1f1f1;
  padding: 10px;
  margin: 0 15px;
  border-radius:25px;
`;

export { SearchContainer }