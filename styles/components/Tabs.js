import styled from 'styled-components/native';

const TabContent = styled.Text`    
    color: ${props => props.active ? 'white' : 'black'};
    font-size: 16px;
    font-family: 'nunitoSemiBold';
`;
const TabContainer = styled.TouchableOpacity`    
    /* height: 30px; */
    margin-top: 15px;
    background: red;
    padding: 10px 20px;
    margin-left: ${props => props.index === 0 ? '15px' : '5px'};
    margin-right: ${props => props.isLastItem ? '15px' : '5px'};
    background: ${props => props.active ? '#fd9926' : '#f1f1f1'};
    border-radius: 20px;
    justify-content: center;
    align-items: center;
`;

export { TabContainer, TabContent }