import styled from 'styled-components/native';

const OfferCardImageContainer = styled.ImageBackground`    
    height: 250px;
    margin: 15px;
    overflow:  hidden;
    border-radius: 30px;
`;
const OfferCardContainer = styled.View`    
    background-color: rgba(0,0,0,.25);
    flex: 1;
    justify-content: center;
    align-items: center;
`;
const OfferDescription = styled.Text`    
    flex: 1;
    padding-top: 20px;
    font-family: 'nunitoBold';
    font-size: 16.5px;
    color: white;
`;
const OfferClickableArea = styled.Text`
    /* text-align: center; */
    justify-content: center;
    align-items: center;
    font-family: 'nunitoBold';
    font-size: 16.5px;
    color: white;
`
const OfferClickableAreaContainer = styled.View`
    flex: 0.3;
    background-color: rgba(255,255,255,0.2);
    justify-content: center;
    align-items: center;
`
export { OfferClickableAreaContainer, OfferClickableArea, OfferCardImageContainer, OfferCardContainer, OfferDescription }