import styled from 'styled-components/native';

const HeadingText = styled.Text`    
    margin-left: 15px;
    color: ${props => props.active ? 'white' : 'black'};
    font-size: 24px;
    font-family: 'nunitoBold';
`;

export { HeadingText }