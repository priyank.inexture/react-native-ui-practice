import styled from 'styled-components/native'
const InputContainer = styled.View` 
    margin-bottom: 15px;
`;
const InputLabel = styled.Text`
    color: black;
    /* margin-top: 10px; */
    margin-left: 12px;
    margin-bottom: 5px;
    font-family: 'nunitoBold';
`;
const TextContainer = styled.View`
    flex: 1;
    flex-direction: row;
    justify-content: space-between;
    background-color: #f1f1f1;
    border-radius: 25px;
`;
const TextInput = styled.TextInput`
    border: none;
    flex: 1;
    font-family: 'nunitoRegular';
    background-color: #f1f1f1;
    border-radius: 25px;
    padding: 15px 14px;
    margin-top: 2px;
`;
export { InputContainer, TextInput, InputLabel, TextContainer }