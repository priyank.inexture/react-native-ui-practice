import styled from 'styled-components/native'

const MenuLogoContainer = styled.View`
    /* flex : 1;
    align-items: flex-flex-start; */
    margin-left: 15px;
    width: 200px;
    height: 30px;
`
const MenuFirstLine = styled.View`
    background-color: black;
    margin-top: 8px;
    margin-bottom: 7px;
    width: 30px;
    height: 3px;
    border-radius: 25px;
`
const MenuSecondLine = styled.View`
    background-color: black;
    width: 20px;
    height: 3px;
    border-radius: 25px;
`
export { MenuLogoContainer, MenuFirstLine, MenuSecondLine }