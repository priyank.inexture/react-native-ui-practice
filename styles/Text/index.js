import styled from 'styled-components/native'
const HyperLinkText = styled.Text`
    flex-direction: row;
    flex-wrap: wrap;
    font-family: 'nunitoRegular';
    color: #76b0ef;
`
const InfoText = styled.Text`
    margin: 10px 0px;
    text-align: center;
    flex-direction: row;
    flex-wrap: wrap;
    font-size: 14px;
`
export { HyperLinkText, InfoText };