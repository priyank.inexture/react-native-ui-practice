import styled from 'styled-components/native'
const FilledButton = styled.TouchableOpacity`
    border-radius: 25px;
    background-color: #fd9926;
`
const PartialFilledButton = styled.TouchableOpacity`
    border-radius: 25px;
    background-color: #fff2e2;
`
const ButtonText = styled.Text`
    font-size: 16px;
    padding: 16px 14px;
	text-align: center;
    color:${props => props.color ? props.color : black};
    font-family: 'nunitoBold';
`
export { PartialFilledButton, FilledButton, ButtonText }