const images = {
	logoIcon: require('./images/amazon_logo.png'),
	cartIcon: require('./images/cart_logo.png'),
	offerBg: require('./images/offer_bg.jpg'),
	chairCategoryImage: require('./images/chair.png'),
	headPhoneCategoryImage: require('./images/headphone.png'),
	watchCategoryImage: require('./images/watch.png'),
	foodCategoryImage: require('./images/food.png'),
};


export default images;