import React, { useState, useRef } from 'react'
import { StyleSheet, ScrollView, View, Text, KeyboardAvoidingView } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons'
import { TextContainer, InfoContainer, ButtonText, InfoText, Container, InputContainer, TextInput, InputLabel, FilledButton, PartialFilledButton, HyperLinkText } from '../styles'
import Button from '../components/Button'
function SignUpScreen({ navigation }) {
    const [enableShift, setEnableShift] = useState(false);
    const [passVisible, setPassVisible] = useState(false);
    const [rePassVisible, setRePassVisible] = useState(false);
    const emailRef = useRef();
    const passwordRef = useRef();
    const rePasswordRef = useRef();
    const handleSignIn = () => navigation.navigate('Home')
    return (
        <ScrollView>
            <KeyboardAvoidingView
                style={{ flex: 1, justifyContent: 'space-evenly', paddingHorizontal: 15, backgroundColor: 'white' }}
                behavior='position'
                enabled={enableShift}
            >

                {/* <Text>Sign up screen</Text>
            <Button onPress={() => navigation.openDrawer()} title='open' /> */}
                <View style={{ marginTop: 0 }}>
                    <InputContainer>
                        <InputLabel>Your name</InputLabel>
                        <TextInput
                            placeholder=''
                            onFocus={() => setEnableShift(false)}
                            onSubmitEditing={() => emailRef.current.focus()}
                            blurOnSubmit={false}
                        />
                    </InputContainer>
                    <InputContainer>
                        <InputLabel>Email</InputLabel>
                        <TextInput
                            placeholder=''
                            onFocus={() => setEnableShift(false)}
                            onSubmitEditing={() => passwordRef.current.focus()}
                            blurOnSubmit={false}
                            ref={emailRef} />
                    </InputContainer>
                    <InputContainer>
                        <InputLabel>Password</InputLabel>
                        <TextContainer>
                            <TextInput
                                placeholder='At least 6 characters'
                                onFocus={() => setEnableShift(true)}
                                onSubmitEditing={() => rePasswordRef.current.focus()}
                                secureTextEntry={!passVisible}
                                blurOnSubmit={false}
                                ref={passwordRef}
                            />
                            <MaterialCommunityIcons
                                name={passVisible ? 'eye' : 'eye-off'}
                                style={styles.icon}
                                onPress={() => passVisible ? setPassVisible(false) : setPassVisible(true)} />
                        </TextContainer>
                    </InputContainer>
                    <InputContainer>
                        <InputLabel>Re-enter password</InputLabel>
                        <TextContainer>
                            <TextInput
                                placeholder='At least 6 characters'
                                onFocus={() => setEnableShift(true)}
                                blurOnSubmit={false}
                                secureTextEntry={!rePassVisible}
                                ref={rePasswordRef}
                            />
                            <MaterialCommunityIcons name={rePassVisible ? 'eye' : 'eye-off'} style={styles.icon}
                                onPress={() => rePassVisible ? setRePassVisible(false) : setRePassVisible(true)} />
                        </TextContainer>
                    </InputContainer>
                </View>
                <View>
                    {/* <FilledButton>
                        <ButtonText color='white'>Register</ButtonText>
                    </FilledButton> */}
                    <Button title="Register" varient="primary" onPress={() => console.log('register')} />
                    <InfoContainer>
                        <InfoText>
                            <Text style={{ flexDirection: 'row', flexWrap: 'wrap' }}>By continuing, you agree to Amazon's</Text>
                            <HyperLinkText>&nbsp;Conditions of Use</HyperLinkText>
                            <Text style={{ flexDirection: 'row', flexWrap: 'wrap' }}>&nbsp;and</Text>
                            <HyperLinkText>&nbsp;Privacy Notice.</HyperLinkText>
                        </InfoText>
                    </InfoContainer>
                    <View style={{
                        alignItems: 'center',
                        marginTop: 15
                    }} >
                        <View style={{
                            borderBottomWidth: 1,
                            borderColor: '#dedede',
                            width: 100
                        }} />
                        <Text style={{ textAlign: 'center', marginVertical: 15 }}>Already have an account?</Text>
                    </View>
                    <Button title="Sign in" onPress={handleSignIn} />
                </View>
            </KeyboardAvoidingView>
        </ScrollView >
    )
}
const styles = StyleSheet.create({
    icon: {
        fontSize: 24,
        marginTop: 17,
        marginRight: 10
    }
});
export default SignUpScreen
