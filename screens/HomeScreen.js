import React, { useState } from 'react'
import { Button, FlatList, View, ScrollView, Text } from 'react-native'
import Tabs from '../components/Tabs';
import OfferCard from '../components/OfferCard';
import CategoryCard from '../components/CategoryCard';
import Heading from '../components/Heading';
import { Container } from '../styles'
import Images from '../assets'
import SearchBar from '../components/SearchBar';
const tabList = ['Home', 'Today\'s Deals', 'Nika\'s Amazon.com', 'Furniture', 'Electronic'];
const categoryList = [
    {
        text: 'Headphones',
        image: Images.headPhoneCategoryImage
    },
    {
        text: 'Furniture',
        image: Images.chairCategoryImage
    },
    {
        text: 'Wriest watch',
        image: Images.watchCategoryImage
    },
    {
        text: 'Food',
        image: Images.foodCategoryImage
    }
];
function HomeScreen({ navigation }) {
    const [activeTab, setActiveTab] = useState(0);
    return (
        <ScrollView style={{ backgroundColor: 'white' }}>
            <SearchBar />
            <FlatList
                showsHorizontalScrollIndicator={false}
                data={tabList}
                horizontal
                keyExtractor={(item, index) => item + index}
                renderItem={({ item, index }) =>
                    <Tabs
                        text={item}
                        index={index}
                        active={index === activeTab}
                        isLastItem={index === tabList.length - 1}
                        onPress={() => setActiveTab(index)}
                    />
                }
            />
            {/* <Text>home screen</Text>
            <Button onPress={() => navigation.navigate('Detail')} title="details" /> */}
            <OfferCard text='30% Off On PC Bundles' onPress={() => console.log('pressed offer')} />
            <Heading text='Top Categories' />
            <FlatList
                showsHorizontalScrollIndicator={false}
                data={categoryList}
                horizontal
                keyExtractor={(item, index) => item + index}
                renderItem={({ item, index }) =>
                    <CategoryCard text={item.text} image={item.image} />
                }
            />
        </ScrollView>
    )
}

export default HomeScreen
