import React, { useState, useEffect, useRef } from 'react';
import { Animated, ScrollView, Picker, Text, StyleSheet, View, Dimensions, Image, StatusBar } from 'react-native'
import Images from '../assets'
import { Container } from '../styles'
import { useHeaderHeight } from '@react-navigation/stack';
import Slider from '../components/slider/Slider'
import Button from '../components/Button'
import Table from '../components/Table'
import { MaterialCommunityIcons } from '@expo/vector-icons';
const { width, height } = Dimensions.get('window');
const slides = [
    { source: Images.watchCategoryImage },
    { source: Images.headPhoneCategoryImage },
    { source: Images.watchCategoryImage }
]
function DetailScreen({ navigation }) {
    const headerHeight = useHeaderHeight();
    const scrollY = useRef(new Animated.Value(0)).current;
    const [selectedValue, setSelectedValue] = useState("black");
    useEffect(() => {
        navigation.setOptions({
            headerShown: true,
            headerTransparent: true,
        });
        scrollY.addListener(({ value }) => {
            if (value < height * 0.05) {
                navigation.setOptions({
                    headerShown: true,
                    headerTransparent: true,
                });
            } else {
                navigation.setOptions({
                    headerShown: true,
                    headerTransparent: false,
                });
            }
        });
    }, []);
    return (
        <Container>
            <Animated.ScrollView
                style={styles.scrollAbleContainer}
                onScroll={Animated.event([
                    {
                        nativeEvent: {
                            contentOffset: {
                                y: scrollY,
                            },
                        },
                    },
                ])}
                showsHorizontalScrollIndicator={false}
                bounces={false}
            >
                <View
                    style={{
                        backgroundColor: 'white',
                        flex: 1,
                        marginTop: (headerHeight),
                        borderTopLeftRadius: 25,
                        borderTopRightRadius: 25
                    }}
                >
                    <Slider slides={slides} />
                    <View style={{
                        paddingHorizontal: 15,
                        marginTop: 15
                    }}>
                        <Text style={styles.productName}>Apple Watch Series 6 Black</Text>
                        <View style={styles.ratingContainer}>
                            <View style={styles.starContainer}>
                                <MaterialCommunityIcons name="star" style={styles.filledStar} />
                                <MaterialCommunityIcons name="star" style={styles.filledStar} />
                                <MaterialCommunityIcons name="star" style={styles.filledStar} />
                                <MaterialCommunityIcons name="star" style={styles.notfilledStar} />
                                <MaterialCommunityIcons name="star" style={styles.notfilledStar} />
                            </View>
                            <Text style={styles.reviews}>(24 122 View)</Text>
                        </View>
                        <Text style={styles.title}>Color</Text>
                        <View style={styles.picker}
                        >
                            <View style={{
                                flex: 0.05,
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}>
                                <View style={{
                                    backgroundColor: selectedValue === 'black' ? 'black' : 'grey',
                                    marginLeft: 25,
                                    width: 25,
                                    height: 25,
                                    borderRadius: 12.5
                                }} />
                            </View>
                            <Picker
                                style={{ flex: 0.90 }}
                                selectedValue={selectedValue}
                                onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
                                itemStyle={{
                                    fontFamily: 'nunitoRegular'
                                }}
                            >
                                <Picker.Item label="Black with stainless" value="black" color="#4f4f4f" />
                                <Picker.Item label="Grey with stainless" value="grey" color="#4f4f4f" />
                            </Picker>
                        </View>
                        <View>
                            <Table />
                        </View>
                    </View>
                </View>
            </Animated.ScrollView>
            <View style={styles.buyContainer}>
                <Text style={styles.location}>Deliver to Tbiisi, Georgia</Text>
                <Button title="Buy now" varient="primary" onPress={() => console.log('buy now')} />
            </View>
        </Container>
    )
}
const styles = StyleSheet.create({
    scrollAbleContainer: {
        flex: 1,
        backgroundColor: '#dedede',
        // backgroundColor: 'yellow',
        height: height * 0.3
    },
    productImageContainer: {
        backgroundColor: 'green',
        height: height * 0.4,
    },
    buyContainer: {
        paddingHorizontal: 15,
        marginVertical: 5,
        backgroundColor: '#f3f5f7',
        borderTopLeftRadius: 25,
        borderTopRightRadius: 25,
        flexDirection: 'column',
        justifyContent: 'center',
        // alignItems: 'center',
        height: height * 0.13
    },
    location: {
        marginLeft: 15,
        marginBottom: 5,
        fontFamily: 'nunitoBold',
    },
    productName: {
        fontSize: 21,
        fontFamily: 'nunitoBold'
    },
    ratingContainer: {
        flexDirection: 'row',
    },
    starContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    filledStar: {
        fontSize: 16,
        color: '#fd9926'
    },
    notfilledStar: {
        fontSize: 16,
        color: '#dedede'
    },
    title: {
        marginTop: 21,
        marginBottom: 5,
        fontSize: 16,
        fontFamily: 'nunitoBold',
        color: '#4f4f4f'
    },
    reviews: {
        color: '#5f83d5'
    },
    picker: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: '#f3f5f7',
        borderRadius: 25
    }
})
export default DetailScreen