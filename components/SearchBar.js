import React from 'react'
import { View, TextInput } from 'react-native'
import { MaterialCommunityIcons, FontAwesome } from '@expo/vector-icons'
import { SearchContainer } from '../styles'
function SearchBar() {
    return (
        <SearchContainer>
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <FontAwesome name="search" style={{ fontSize: 20 }} />
            </View>
            <View style={{ flex: 5 }}>
                <TextInput placeholder="Search here!" />
            </View>
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <MaterialCommunityIcons name="barcode-scan" style={{ fontSize: 20 }} />
            </View>
        </SearchContainer>
    )
}

export default SearchBar
