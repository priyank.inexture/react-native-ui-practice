import React from 'react'
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native'
import { BlurView } from 'expo-blur';
import { MaterialCommunityIcons } from '@expo/vector-icons'
import { OfferCardImageContainer, OfferCardContainer, OfferDescription } from '../styles'
import Images from '../assets'
function OfferCard({ text, onPress }) {
    return (
        <OfferCardImageContainer source={Images.offerBg}>
            <OfferCardContainer>
                <OfferDescription>{text}</OfferDescription>
            </OfferCardContainer>
            <BlurView tint='light' intensity={25} style={styles.blurView}>
                <TouchableOpacity onPress={onPress} style={styles.touchableOpacity}>
                    <Text style={styles.blurOverText}>
                        See offers
                </Text>
                    <MaterialCommunityIcons name="chevron-right" style={styles.arrow} />
                </TouchableOpacity>
            </BlurView>
            {/* <OfferClickableAreaContainer>
                
            </OfferClickableAreaContainer> */}
        </OfferCardImageContainer>

    )
}
const styles = StyleSheet.create({
    touchableOpacity: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    blurView: {
        flex: 0.3,
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
    },
    blurOverText: {
        paddingLeft: 20,
        fontFamily: 'nunitoBold',
        fontSize: 16.5,
        justifyContent: 'center',
        alignItems: 'center',
        color: 'white'
    },
    arrow: {
        marginTop: 3,
        marginLeft: 5,
        fontSize: 24,
        color: 'white'
    }
})
export default OfferCard
