import React from 'react'
import { View, TouchableOpacity } from 'react-native'
import { ButtonText } from '../styles'
function Button({ onPress, title, varient }) {
    return (
        <TouchableOpacity onPress={onPress}>
            <View style={{
                borderRadius: 25,
                backgroundColor: varient === 'primary' ? '#fd9926' : '#fff2e2'
            }}>
                <ButtonText color={varient === 'primary' ? '#ffffff' : '#fd9926'}>{title}</ButtonText>
            </View>
        </TouchableOpacity>
    )
}

export default Button
