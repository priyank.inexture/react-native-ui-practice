import React from 'react'
import { HeadingText } from '../styles'
function Heading({ text }) {
    return (
        <HeadingText>{text}</HeadingText>
    )
}

export default Heading
