import React from 'react'
import { TabContainer, TabContent } from '../styles'
function Tabs({ text, active, index, isLastItem, onPress }) {
    return (

        <TabContainer
            onPress={onPress} pressDuration={0.1}
            active={active} index={index} isLastItem={isLastItem}>
            <TabContent active={active}>{text ? text : 'Tab'}</TabContent>
        </TabContainer>
    )
}

export default Tabs
