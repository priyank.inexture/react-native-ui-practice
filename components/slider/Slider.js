import React, { useRef } from 'react'
import { View, ScrollView, Dimensions, Image, Animated } from 'react-native'
import Dot from './Dot'
const { width, height } = Dimensions.get('window')
const Slide = ({ source }) => {
    return <Image source={source} style={{
        height: height * 0.35,
        width: width,
        resizeMode: 'contain',
        backgroundColor: '#dedede',
    }} />
}

function Slider({ slides }) {
    const scrollX = useRef(new Animated.Value(0)).current;
    return (
        <View style={{
            backgroundColor: '#dedede',
            borderBottomLeftRadius: 25,
            borderBottomRightRadius: 25
        }}>
            <Animated.ScrollView
                horizontal
                snapToInterval={width}
                onScroll={Animated.event([
                    {
                        nativeEvent: {
                            contentOffset: {
                                x: scrollX,
                            },
                        },
                    },
                ])}
                decelerationRate="fast"
                showsHorizontalScrollIndicator={false}
                bounces={false}
            >
                {slides.map((s, index) => <Slide key={index} source={s.source} />)}
            </Animated.ScrollView>
            <View style={{
                // backgroundColor: 'cyan',
                width: width,
                height: height * 0.04,
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center'
            }}>
                {slides.map((_, index) => (
                    <Dot
                        key={index}
                        index={index}
                        currentIndex={Animated.divide(scrollX, width)}
                    />
                ))}

            </View>
        </View>
    )
}

export default Slider
