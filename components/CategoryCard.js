import React from 'react'
import { Image, Text, StyleSheet, View, TouchableOpacity } from 'react-native'
import { CategorCardContainer, CategoryImage } from '../styles'
function CategoryCard({ text, image }) {
    return (
        <TouchableOpacity style={{ alignItems: 'center', marginBottom: 10 }}>
            <CategorCardContainer>
                <Image source={image} style={style.image} />
                <Text style={style.categoryName}>{text}</Text>
            </CategorCardContainer>
            <View style={{ position: 'relative' }}>
                <View style={style.halfBackground} />
            </View>
        </TouchableOpacity>
    )
}
const style = StyleSheet.create({
    categoryName: {
        fontFamily: 'nunitoSemiBold',
        fontSize: 18,
        color: 'black'
    },
    imgContainer: {
        flexDirection: 'row'
    },
    image: {
        height: 180,
        marginRight: 15,
        resizeMode: 'contain',
        flex: 1,
        aspectRatio: 1
    },
    halfBackground: {
        width: 170,
        height: 190,
        position: 'absolute',
        top: -200,
        left: -90,
        zIndex: -1,
        backgroundColor: '#f3f5f7',
        borderRadius: 25,
    }
});
export default CategoryCard
