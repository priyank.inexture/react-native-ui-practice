import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
const fields = [
    { title: 'Supported Application', text: 'Email, Gps, Heart Rate Monitor' },
    { title: 'Brand', text: 'Apple' },
    { title: 'Size', text: '1.2"' },
    { title: 'Wireless Communication Standard', text: '802.11g/n' }
]
function Table() {
    return (
        <View style={{ flex: 1, flexDirection: 'column', paddingTop: 15 }}>
            {
                fields.map(f => (
                    <View style={styles.container}>
                        <Text style={styles.title}>{f.title}</Text>
                        <Text style={styles.text}>{f.text}</Text>
                    </View>)
                )
            }
            {
                fields.map(f => (
                    <View style={styles.container}>
                        <Text style={styles.title}>{f.title}</Text>
                        <Text style={styles.text}>{f.text}</Text>
                    </View>)
                )
            }
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        marginVertical: 2
    },
    title: {
        fontFamily: 'nunitoBold',
        flex: 0.5,
        color: '#b3b3b3'
    },
    text: {
        fontFamily: 'nunitoRegular',
        flex: 0.5,
        color: '#4f4f4f'
    }
})
export default Table