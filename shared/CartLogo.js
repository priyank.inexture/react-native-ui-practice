import React from 'react';
import { Image } from 'react-native'
import Images from '../assets'
import { CartHeaderLogo } from '../styles';
function CartLogo(props) {
    return (
        <CartHeaderLogo
            source={Images.cartIcon}
        />
    );
}
export default CartLogo;