import React from 'react';
import { Image } from 'react-native'
import Images from '../assets'
import { AmazonHeaderLogo } from '../styles';
function HeaderLogo() {
    return (
        <AmazonHeaderLogo
            source={Images.logoIcon}
        />
    );
}
export default HeaderLogo;