import React from 'react'
import { TouchableWithoutFeedback } from 'react-native';
import { MenuLogoContainer, MenuFirstLine, MenuSecondLine } from '../styles'
function MenuLogo({ onPress }) {
    console.log(onPress);
    return (
        <TouchableWithoutFeedback onPress={onPress}>
            <MenuLogoContainer >
                <MenuFirstLine />
                <MenuSecondLine />
            </MenuLogoContainer>
        </TouchableWithoutFeedback>
    )
}

export default MenuLogo;
